//
//  BMHSimulatorPhotoImporter.m
//  iPhoneSimulatorPhotoImporter
//
//  Created by Bradley Handy on 10/10/12.
//  Copyright (c) 2012 Bradley Handy. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import "BMHSimulatorPhotoImporter.h"

NSString* const JPEG_LONG_EXTENSION = @"jpeg";
NSString* const JPEG_SHORT_EXTENSION = @"jpg";
NSString* const TIFF_LONG_EXTENSION = @"tiff";
NSString* const TIFF_SHORT_EXTENSION = @"tif";
NSString* const PNG_EXTENSION = @"png";
NSString* const GIF_EXTENSION = @"gif";

@interface BMHSimulatorPhotoImporter ()

- (id)initWithImportPath:(NSString*)path options:(int)options;

- (void)startImporting;
- (void)importNextPhoto;
- (void)importPhotoAtPath:(NSString*)path;

- (NSPredicate*)createFileTypesPredicate:(int)options;
- (NSString*)nextPath;

@end

@implementation BMHSimulatorPhotoImporter {
    ALAssetsLibrary* __library;
    NSEnumerator* __fileEnumerator;
    NSPredicate* __fileTypePredicate;
    NSString* __rootPath;
    
    BOOL __deleteOriginal;
}

+ (void)importImagesFrom:(NSString *)path withOptions:(int)options {
    [[[BMHSimulatorPhotoImporter alloc] initWithImportPath:path options:options] startImporting];
}

- (id)initWithImportPath:(NSString *)path options:(int)options {
    if (( self = [super init] )) {
        NSFileManager* fileManager = [NSFileManager defaultManager];
        NSError* error = nil;
        
        __library = [[ALAssetsLibrary alloc] init];
        __deleteOriginal = options & ImporterDeleteOriginalPhotoOption;
        __fileTypePredicate = [self createFileTypesPredicate:options];
        __rootPath = path;
        __fileEnumerator = (options & ImporterRecursiveSearchOption)?
                [fileManager enumeratorAtPath:path]
                : [[fileManager contentsOfDirectoryAtPath:path error:&error] objectEnumerator];
        
        if (error) {
            NSLog(@"Error while reading photo directory:  %@", [error description]);
            return nil;
        }
    }
    return self;
}

- (void)startImporting {
    NSLog(@"Starting photo import...");
    [self importNextPhoto];
}

- (void)importNextPhoto {
    [self importPhotoAtPath:[self nextPath]];
}

- (void)importPhotoAtPath:(NSString *)path {
    if (path) {
        NSLog(@"- Importing '%@'...", path);
        NSData* fileData = [[NSFileManager defaultManager] contentsAtPath:path];
        
        [__library writeImageDataToSavedPhotosAlbum:fileData metadata:nil
                completionBlock:^(NSURL *assetURL, NSError *error) {
                    if (!error) {
                        NSLog(@"-- URL: %@", assetURL);
                        
                        if (__deleteOriginal) {
                            NSLog(@"-- Deleting %@...", path);
                            NSError* error = nil;
                            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
                            
                            if (error) {
                                NSLog(@"--- Failed deleting file %@", path);
                            }
                        }
                        
                        [self importNextPhoto];
                    } else if ([error code] == ALAssetsLibraryWriteBusyError) {
                        NSLog(@"-- Write busy error encountered.  Retrying image '%@'...", path);
                        [self importPhotoAtPath:path];
                    } else {
                        NSLog(@"-- Fatal import error occurred while processing '%@': %@", path, [error description]);
                    }
                }];
    }
}

- (NSPredicate *)createFileTypesPredicate:(int)options {
    NSMutableArray* fileTypes = [[NSMutableArray alloc] init];
    
    if (options & ImporterIncludeJPEGOption) {
        [fileTypes addObject:JPEG_LONG_EXTENSION];
        [fileTypes addObject:JPEG_SHORT_EXTENSION];
    }
    
    if (options & ImporterIncludeTIFFOption) {
        [fileTypes addObject:TIFF_LONG_EXTENSION];
        [fileTypes addObject:TIFF_SHORT_EXTENSION];
    }
    
    if (options & ImporterIncludePNGOption) {
        [fileTypes addObject:PNG_EXTENSION];
    }
    
    if (options & ImporterIncludeGIFOption) {
        [fileTypes addObject:GIF_EXTENSION];
    }
    
    return [NSPredicate predicateWithFormat:@"lowercasePathExtension in %@", fileTypes];
}

- (NSString *)nextPath {
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* currentPath = nil;
    NSString* path = nil;
    BOOL directory;
    
    while (( currentPath = [__fileEnumerator nextObject] )) {
        NSString* filePath = [__rootPath stringByAppendingFormat:@"/%@", currentPath];
        NSLog(@"Checking %@...", filePath);
        
        if (([fileManager fileExistsAtPath:filePath isDirectory:&directory] && directory)
                || ![__fileTypePredicate evaluateWithObject:filePath]) {
            continue;
        }
        
        NSLog(@"- Found picture to import:  %@", filePath);
        path = filePath;
        break;
    }
    
    return path;
}

@end

@interface NSString (CaseInsensitivePathExtension)

- (NSString*)lowercasePathExtension;

@end

@implementation NSString (CaseInsensitivePathExtension)

- (NSString *)lowercasePathExtension {
    return [[self pathExtension] lowercaseString];
}

@end
