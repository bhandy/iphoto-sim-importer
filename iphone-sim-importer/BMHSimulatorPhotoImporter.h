//
//  BMHSimulatorPhotoImporter.h
//  iPhoneSimulatorPhotoImporter
//
//  Created by Bradley Handy on 10/10/12.
//  Copyright (c) 2012 Bradley Handy. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    ImporterOptionsNone = 0,
    ImporterDeleteOriginalPhotoOption = 1,   // deletes the original files upon successful import.
    ImporterRecursiveSearchOption = 2,       // descends into child directories looking for images to import.
    ImporterIncludeJPEGOption = 4,           // looks for files with .jpg or .jpeg, case insensitive
    ImporterIncludeTIFFOption = 8,           // looks for files with .tiff and .tif, case insensitive
    ImporterIncludePNGOption = 16,           // looks for files with .png, case insensitive
    ImporterIncludeGIFOption = 32,           // looks for files with .gif, case insensitive
    ImporterIncludeAllImageTypesOption = 60
} ImportOptions;

@interface BMHSimulatorPhotoImporter : NSObject

+ (void)importImagesFrom:(NSString*)path withOptions:(int)options;

@end
